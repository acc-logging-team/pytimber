import pytest
import pytimber

@pytest.fixture(scope="session")
def nxcals():
    return pytimber.NXCals()
