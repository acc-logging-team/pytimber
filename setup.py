"""
setup.py for pytimber.

For reference see
https://packaging.python.org/guides/distributing-packages-using-setuptools/

"""
from pathlib import Path
from setuptools import setup, find_packages

from setuptools_scm import get_version


HERE = Path(__file__).parent.absolute()
with (HERE / 'README.md').open('rt') as fh:
    LONG_DESCRIPTION = fh.read().strip()


REQUIREMENTS: dict = {
    'core': [
        "python-dateutil",
        "jpype1~=1.1",
        "cmmnbuild-dep-manager>=2.7.0",
        "matplotlib",
        "numpy",
        "pytz",
        "scipy",
    ],
    'full': [
        'pandas',
    ],
    'test': [
        'pytest',
        'pandas',
    ],
}


# Get the version from setuptools_scm using the same scheme defined
# in pyproject.toml. Note that we use a no-local-version scheme
# (where versions are defined as "X.Y.Z.devN[+local-scheme]") so that
# the entry-point follows a valid entry-point specification as per
# https://packaging.python.org/specifications/entry-points/.
VERSION = get_version(local_scheme="no-local-version")


setup(
    name="pytimber",
    version=VERSION,
    description="A Python wrapping of CALS API",
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',

    maintainer="CERN BE-CO-APS",
    maintainer_email="acc-logging-support@cern.ch",
    author="Riccardo De Maria",
    author_email="riccardo.de.maria@cern.ch",
    url="https://gitlab.cern.ch/acc-logging-team/pytimber",
    packages=find_packages(exclude="tests"),

    python_requires="~=3.6",
    install_requires=REQUIREMENTS['core'],
    extras_require={
        **REQUIREMENTS,
        # The 'dev' extra is the union of 'test' and 'doc', with an option
        # to have explicit development dependencies listed.
        'dev': [req
                for extra in ['dev', 'test', 'doc']
                for req in REQUIREMENTS.get(extra, [])],
        # The 'all' extra is the union of all requirements.
        'all': [req for reqs in REQUIREMENTS.values() for req in reqs],
    },

    entry_points={
        # Register with cmmnbuild_dep_manager.
        "cmmnbuild_dep_manager": [f"pytimber={VERSION}"],
    },
)
