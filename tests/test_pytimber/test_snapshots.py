import pytest

from pytimber.snapshots import SnapshotSearchCriteria


@pytest.mark.integration
class TestIntegration:

    @pytest.mark.parametrize(
        "snapshot_pattern, owner_pattern, description_pattern, count_snapshots",
        [
            ("FILL%", "psowins%", None, 2),
            ("%", "psowinsk", "Fills comparison", 1)
        ]
    )
    def test_get_snapshot_names(self, ldb, snapshot_pattern, owner_pattern, description_pattern, count_snapshots):
        result = ldb.getSnapshotNames(snapshot_pattern, owner_pattern, description_pattern)

        assert len(result) == count_snapshots

    @pytest.mark.core
    @pytest.mark.parametrize(
        "snapshot_pattern, owner_pattern, snapshot, variable, count_snapshots, count_variables",
        [
            ("FILL%", "psowins%", "FILLS2", "LHC.STATS:FILL_NUMBER", 2, 4)
        ]
    )
    def test_get_variables_for_snapshots_as_pattern(
            self, ldb, snapshot_pattern, owner_pattern, snapshot, variable, count_snapshots, count_variables
    ):

        result = ldb.getVariablesForSnapshots(snapshot_pattern, owner_pattern)

        assert variable in result[snapshot]
        assert len(result) == count_snapshots

        total_variables = 0
        for snapshot in result:
            total_variables += len(result[snapshot])

        assert total_variables == count_variables
        assert variable in result[snapshot]

    @pytest.mark.core
    @pytest.mark.parametrize(
        "snapshot_list, owner_pattern, snapshot, variable, count_variables",
        [
            (["FILLS", "FILLS2"], "psowinsk", "FILLS", "LHC.STATS:FILL_NUMBER", 4)
        ]
    )
    def test_get_variables_for_snapshots_as_list(
            self, ldb, snapshot_list, owner_pattern, snapshot, variable, count_variables
    ):
        result = ldb.getVariablesForSnapshots(snapshot_list, owner_pattern)

        total_variables = 0
        for node in result:
            total_variables += len(result[node])

        assert total_variables == count_variables
        assert variable in result[snapshot]

    @pytest.mark.core
    @pytest.mark.parametrize(
        "snapshot_pattern_or_list, owner_pattern, snapshot_name, variable_name, values_count",
        [
            (["FILLS", "FILLS2", "FILLS4"], "psowinsk", 'FILLS', "HX:FILLN", 977)
        ]
    )
    def test_get_data_with_snapshots_using_definitions(
            self, ldb, snapshot_pattern_or_list, owner_pattern, snapshot_name, variable_name, values_count
    ):
        result = ldb.getDataUsingSnapshots(snapshot_pattern_or_list, owner_pattern)

        assert len(result[snapshot_name][variable_name][1]) == values_count

    @pytest.mark.core
    @pytest.mark.parametrize(
        "snapshot_pattern_or_list, owner_pattern, search_criteria, snapshot_name, variable_name, values_count",
        [
            (["FILLS", "FILLS2", "FILLS4"], "psowinsk",
             SnapshotSearchCriteria(None, "2022-01-01 00:00:00.000", "2022-12-31 23:59:00.000"),
             "FILLS", "HX:FILLN", 977),
            ("FUND", "psowinsk",
             SnapshotSearchCriteria(None, "2015-05-15 12:00:00.000", "2015-05-15 12:01:00.000"),
             "FUND", "CPS.TGM:USER", 39),
            ("FUND", "psowinsk",
             SnapshotSearchCriteria("CPS:%:SFTPRO%", "2015-05-15 12:00:00.000", "2015-05-15 12:01:00.000"),
             "FUND", "CPS.TGM:USER", 6),
        ]
    )
    def test_get_data_with_snapshots_using_search_criteria(
            self, ldb, snapshot_pattern_or_list, owner_pattern,
            search_criteria, snapshot_name, variable_name, values_count
    ):
        result = ldb.getDataUsingSnapshots(snapshot_pattern_or_list, owner_pattern, search_criteria=search_criteria)

        assert len(result[snapshot_name][variable_name][1]) == values_count
