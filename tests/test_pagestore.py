from pytimber.pagestore import PageStore
import numpy as np
import pytest


@pytest.fixture
def db(tmp_path):
    pagestore = PageStore("test.db",  str(tmp_path))
    try:
        yield pagestore
    finally:
        pagestore.delete()


@pytest.mark.parametrize(
    "data",
    [
        {"v1": ([1, 2, 3], [1, 2, 3]), "v2": ([1, 2, 1640995205], [4, 5, 6])},
        {"v1": ([1, 2, 333], [1, 2, 3]), "v2": ([1, 2, 654365], [6786, 4543, 765756])}
    ]
)
def test_get(db, data):
    db.store(data)
    data1 = db.get(["v1", "v2"])
    data2 = db.get("v%")

    assert len(data1) == 2
    assert len(data2) == 2

    assert set(data.keys()) == set(data1.keys())

    assert np.array_equal(data["v1"][0], data1["v1"][0])
    assert np.array_equal(data["v2"][1], data2["v2"][1])


@pytest.mark.parametrize(
    "data",
    [
        {"v1": ([1, 2, 3], [1, 2, 3]), "v2": ([1, 2, 1640995205], [4, 5, 6])},
        {"v1": ([1, 2, 333], [1, 2, 3]), "v2": ([1, 2, 654365], [6786, 4543, 765756])}
    ]
)
def test_idx(db, data):
    db.store(data)
    data1 = db.get_idx("v1")
    data2 = db.get_idx("v2")

    assert np.array_equal(data["v1"][0], data1)
    assert np.array_equal(data["v2"][0], data2)
