from typing import Any, Tuple


class Fundamentals:
    def __init__(self, fundamental_pattern: str,
                 dest_in: Tuple[str] = None, dest_not_in: Tuple[str] = None):

        self.fundamental_pattern = fundamental_pattern
        self.dest_in = dest_in
        self.dest_not_in = dest_not_in

    def apply_filter(self, dataset: Any) -> Any:
        if self.dest_in:
            dataset = dataset.where(
                "DEST in (" + ','.join(f'\'{item}\'' for item in self.dest_in) + ")")

        if self.dest_not_in:
            dataset = dataset.where(
                "DEST not in (" + ','.join(f'\'{item}\'' for item in self.dest_not_in) + ")")

        return dataset
